public class Exercise2 {
    public static void main(String[] args) {
        System.out.println("Factorial of 5 is "+fac(5, false));
        System.out.println("Double factorial of 5 is "+fac(5, true));

        System.out.println("Factorial of 10 is "+fac(10, false));
        System.out.println("Double factorial of 10 is "+fac(10, true));

        // StackOverflowError because int overflow
        System.out.println("Factorial of 11 is "+fac(11, false));
        System.out.println("Double factorial of 11 is "+fac(11, true));
    }

    /**
     * Overloaded starting function as interface to the actual recursive function
     * @param number Number to calculate the factorial of
     * @param doubleFac If we calc the double or single factorial
     */
    public static int fac(int number, boolean doubleFac) {
        return fac(1, number, doubleFac);
    }

    /**
     * Recursively calculates the (double) factorial of a given number
     * @param product Running product for the iterations (default=1)
     * @param number Number to calculate the factorial of
     * @param doubleFac If we calc the double or single factorial
     */
    public static int fac(int product, int number, boolean doubleFac) {
        // We use ternary operator here, which is basically just a short way
        // to write an if else branch. (*condition* ? *cond==true* : *cond==false*)
        return number == 1 ? product : fac(
                product * number,
                doubleFac ? number - 2 : number - 1,
                doubleFac
        );
    }
}
