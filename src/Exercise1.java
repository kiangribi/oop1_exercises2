import java.util.Arrays;

public class Exercise1 {
    public static void main(String[] args) {
        int[] arr1 = {1, 2, 3, 4, 5};
        int[] arr2 = {4, 2, 5, 7, 8};
        int[] arr3 = {9, 8, 7, 6, 5};
        System.out.printf("Array 1: %s\n", Arrays.toString(arr1));
        System.out.printf("Array 2: %s\n", Arrays.toString(arr2));
        System.out.printf("Array 3: %s\n\n", Arrays.toString(arr3));
cd
        print(arr1);
        print(arr2);
        print(arr3);
        System.out.println();

        System.out.printf("Array 1 Reverted: %s\n", Arrays.toString(revert(arr1)));
        System.out.printf("Array 2 Reverted: %s\n", Arrays.toString(revert(arr2)));
        System.out.printf("Array 3 Reverted: %s\n\n", Arrays.toString(revert(arr3)));

        System.out.printf("Array 1 ascendinglySorted: %b\n", sorted(arr1, false));
        System.out.printf("Array 2 ascendinglySorted: %b\n", sorted(arr2, false));
        System.out.printf("Array 3 ascendinglySorted: %b\n\n", sorted(arr3, false));

        System.out.printf("Array 1 descendinglySorted: %b\n", sorted(arr1, true));
        System.out.printf("Array 2 descendinglySorted: %b\n", sorted(arr2, true));
        System.out.printf("Array 3 descendinglySorted: %b\n\n", sorted(arr3, true));
    }

    /**
     * Alternative function to manually print an array
     */
    public static void print(int[] arr) {
        System.out.print("Array: [");
        for (int e : arr) {
            // Checks if element is last element of array to not print a comma
            if (e != arr[arr.length - 1]) {
                System.out.printf("%d, ", e);
            } else {
                System.out.print(e);
            }
        }
        System.out.println("]");
    }

    public static int[] revert(int[] arr) {
        int[] result = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            // Get the index of the old array backwards
            // e.g. with arr.length == 5
            // [0] -> [5]
            // [1] -> [4]
            // [2] -> [3]
            // [3] -> [2]
            result[i] = arr[arr.length - 1 - i];
        }
        return result;
    }

    public static boolean sorted(int[] arr, boolean descending) {
        // We use Integer class here to be able to identify define
        // a starting state (null) because "0" would be smaller than a lot of
        // numbers even though it is not actually in the array itself.
        Integer lastChecked = null;
        for (int e : arr) {
            // (descending == true) -> e > lastChecked -> Check if new element is larger
            // (descending == false) -> e < lastChecked -> Check if new element is smaller
            if (lastChecked != null && ((!descending && e < lastChecked) || (descending && e > lastChecked))) {
                return false;
            } else {
                lastChecked = e;
            }
        }
        return true;
    }
}
