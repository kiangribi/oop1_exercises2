import java.util.Arrays;
import java.util.stream.IntStream;

public class Exercise4 {
    public static final int EMPTY_NUMBER = -1;
    public static final int HIGHEST_NUMBER = 20;

    public static void main(String[] args) {
        int[] arr = IntStream.rangeClosed(2, HIGHEST_NUMBER).toArray();
        System.out.println("Array: "+Arrays.toString(arr));
        sieve(arr);
        System.out.println("Sieved Array: "+arrayToString(arr));
        System.out.println("Sieved Array (oldschool print): "+arrayToStringOldSchool(arr));
    }

    public static String arrayToString(int[] arr) {
        return Arrays.toString(Arrays.stream(arr).filter(elem -> elem != -1).toArray());
    }

    /**
     * Alternative toString function that removes all the -1s with
     * classic C-array functions
     * @param arr The array to convert to a string
     */
    public static String arrayToStringOldSchool(int[] arr) {
        // Create a new array with the length equal to the count
        // of all numbers != -1 in the array
        int primeCount = 0;
        for (int i : arr) {
            if (i != -1) primeCount++;
        }
        int[] result = new int[primeCount];
        int index = 0;
        for (int i : arr) {
            if (i != -1) {
                result[index] = i;
                index++;
            }
        }

        // Iteratively build up a string with a Java built-in tool
        StringBuilder sb = new StringBuilder("[");
        for (int e : result) {
            sb.append(e == result[result.length - 1] ? e : e + ", ");
        }

        return sb.append("]").toString();
    }


    public static void sieve(int[] arr) {
        for (int i = 2; i < arr.length; i++) {
            sieveNumber(arr, i);
        }
    }

    public static void sieveNumber(int[] arr, int number) {
        int numberToRemove = number + number;
        while(numberToRemove <= HIGHEST_NUMBER) {
            arr[numberToRemove - 2] = EMPTY_NUMBER;
            numberToRemove += number;
        }
    }
}
