import java.util.Arrays;

public class Exercise3 {
    public static void main(String[] args) {
        System.out.printf("Sum of array [1, 2, 3, 4] is %s\n", sum(new int[]{1, 2, 3, 4}));
        System.out.printf("Sum of array [100, 200, 300] is %s\n", sum(new int[]{100, 200, 300}));
        System.out.printf("Sum of array [343434, 667, 4545] is %s\n", sum(new int[]{343434, 667, 4545}));
    }

    /**
     * Overloaded starting function as interface to the actual recursive function
     * @param arr Array to calculate the sum of
     */
    public static int sum(int[] arr) {
        return sum(0, arr);
    }

    /**
     * Recursively calculates the sum of all elements in an array
     * @param sum Running sum for the iterations (default=0)
     * @param arr Array to calculate the sum of
     */
    public static int sum(int sum, int[] arr) {
        // We use ternary operator here, which is basically just a short way
        // to write an if else branch. (*condition* ? *cond==true* : *cond==false*)
        return arr.length == 0 ? sum : sum(
                sum + arr[arr.length - 1],
                Arrays.copyOfRange(arr, 0, arr.length > 1 ? arr.length - 1 : 0)
        );
    }
}
